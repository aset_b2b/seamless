
var Main = {};

var player;

Main.onLoad = function (){	
	"use strict";
	 if (window.tizen === undefined) {
        
         setTimeout(
         		function(){
         			 Main.log('This application needs to be run on Tizen device');
         		},
         		1000
         );
     }else{
    	setTimeout(
    		function(){
    			
    			Main.log('Started Example');
    			Main.registAllKeys();
    			player = new Seamless();
    			
    			player.init();
    			player.addPlaylistElement("file:///opt/usr/apps/"+tizen.application.getAppInfo().packageId+"/res/wgt/content/red.mp4");
    			player.addPlaylistElement("file:///opt/usr/apps/"+tizen.application.getAppInfo().packageId+"/res/wgt/content/blue.mp4");
    			player.addPlaylistElement("file:///opt/usr/apps/"+tizen.application.getAppInfo().packageId+"/res/wgt/content/yellow.mp4");
    			player.setRectangleArea(0,0,1920,1080);
    			
    			player.play();
    			
    			document.addEventListener('keydown', Main.onKeyDown, false);
    	
    		},
    		5000
    	);
     }
	
};


/**
 * Handles a key press
 * @param {KeyboardEvent} event
 */
Main.onKeyDown = function (event) {
	"use strict";
	var keyCode = event.keyCode;
    
	switch (keyCode) {
		case 49:    // Enter
			player.stop();
	    break;
		case 50:    // Enter
			player.pause();
	    break;
		case 51:    // Enter
			player.play();
	    break;
	    case 413:   // MediaStop 
	    break;
	    case 417:   // MediaFastForward
	    break;
	    case 412:   // MediaRewind
	    break;
	    case 10009: // Return
        break;
		case 38: 
		break;
		case 40:
		break;
		default:
			Main.log(keyCode);
		break;
	}
};


/**
 * Adds text to a UI element -  NOTE: THIS FUNCTION SHOULD NOT BE USED ON PRODUCTION APPS, it is only for visual demonstration of what is happening
 * @param {String} text
 */
Main.log = function (text) {
  "use strict";
  console.log(text);
};


/**
 * Generic Error handler for uncaught JS errors
 * @param errMsg
 * @param url
 * @param lineNum
 * @returns {boolean}
 */
window.onerror = function handleError(errMsg, url, lineNum) {
	"use strict";
  Main.log('Error Message: ' + errMsg);
  Main.log('  URL: ' + url);
  Main.log('  Line: ' + lineNum);
  return false;
};


function onHide()
{   
	player.stop();
}

Main.registAllKeys = function () {
	"use strict";
	try {
        tizen.tvinputdevice.getSupportedKeys()
            .forEach(function (k){
                if ([
                    'ColorF0Red',
                    'ColorF1Green',
                    'ColorF2Yellow',
                    'ColorF3Blue',
                    'MediaFastForward',
                    'MediaPause',
                    'MediaPlay',
                    'MediaPlayPause',
                    'MediaRecord',
                    'MediaRewind',
                    'MediaStop',
                    'Info',
                    '1',
                    '2',
                    '3',
                    '4'
                ].indexOf(k.name) > -1) {
                    tizen.tvinputdevice.registerKey(k.name);
                    Main.log('Subscribed key:'+ k.name + ' ' + k.code);
                }
            });
    } catch (e) {
        Main.log('Could not subscribe keys, exception occurred:'+ e);
    }

	
};


Main.onUnLoad = function (){
	onHide();
	
};



