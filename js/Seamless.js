/**
 * SRT - BRAVO
 * @created by Mario Cecena, mario.cecena@samsung.com
 * */

var Seamless = function (){};

Seamless.prototype.obj1 = null;
Seamless.prototype.obj2 = null;

Seamless.prototype.listener1 = null;
Seamless.prototype.listener2 = null;

Seamless.prototype.content = [];
Seamless.prototype.index = 0;
Seamless.prototype.isPlaying = false;
Seamless.prototype.currentObj = 1;

Seamless.prototype.rectangleCoordenateX = null;
Seamless.prototype.rectangleCoordenateY = null;
Seamless.prototype.rectangleWidth = null;
Seamless.prototype.rectangleHeight = null;


Seamless.prototype.init = function(){
	this.obj1 = webapis.avplaystore.getPlayer();
	this.obj2 = webapis.avplaystore.getPlayer();
	var that =  this;
	that.currentObj = 1;

	this.listener1 = {
			  onbufferingstart: function() {
				  console.log("Buffering start.");
			  },
			  onbufferingprogress: function(percent) {  
				  console.log("Buffering progress data : " + percent);
			  },
			  onbufferingcomplete: function() {
				  console.log("Buffering complete.");
			  },
			  onstreamcompleted: function() {
				  	that.obj2.stop();
					that.currentObj = 1;
					console.log('open1: '+that.content[that.index]);
					console.log('open status: '+that.obj1.open(that.content[that.index]));
					that.index++;
					if(that.index > that.content.length-1){
						that.index=0;
					}
					console.log('setListener status: '+that.obj1.setListener(that.listener2));
					console.log('mixbuffer set: '+that.obj1.setStreamingProperty("USE_VIDEOMIXER"));
					console.log('prepare status: '+that.obj1.prepare());
					console.log('frame set: '+that.obj1.setStreamingProperty("SET_MIXEDFRAME"));
					that.obj1.setDisplayRect(that.rectangleCoordenateX, that.rectangleCoordenateY, that.rectangleWidth, that.rectangleHeight);
					console.log('play status: '+that.obj1.play());
					
			  },
			  onevent: function(eventType, eventData) {
				  console.log("event type error : " + eventType + ", data: " + eventData);
			  },
			  onerror: function(eventType) {
				  console.log("event type error : " + eventType);
			  }
	};
	
	this.listener2 = {
			  onbufferingstart: function() {
				  console.log("Buffering start.");
			  },
			  onbufferingprogress: function(percent) {  
				  console.log("Buffering progress data : " + percent);
			  },
			  onbufferingcomplete: function() {
				  console.log("Buffering complete.");
			  },
			  onstreamcompleted: function() {
				  	that.obj1.stop();
					that.currentObj = 2;

					console.log('open2: '+that.content[that.index]);
					console.log('open status: '+that.obj2.open(that.content[that.index]));
					that.index++;
					if(that.index > that.content.length-1){
						that.index=0;
					}
					console.log('setListener status: '+that.obj2.setListener(that.listener1));
					console.log('mixbuffer set: '+that.obj2.setStreamingProperty("USE_VIDEOMIXER"));
					console.log('prepare status: '+that.obj2.prepare());
					console.log('frame set: '+that.obj1.setStreamingProperty("SET_MIXEDFRAME"));
					that.obj2.setDisplayRect(that.rectangleCoordenateX, that.rectangleCoordenateY, that.rectangleWidth, that.rectangleHeight);
					console.log('play status: '+that.obj2.play());
			  },
			  onevent: function(eventType, eventData) {
				  console.log("event type error : " + eventType + ", data: " + eventData);
			  },
			  onerror: function(eventType) {
				  console.log("event type error : " + eventType);
			  }
	};
};


Seamless.prototype.stop = function(){
	this.isPlaying = false;
	this.obj1.stop();
	this.obj2.stop();
};

Seamless.prototype.clearPlaylist = function(){
	
	if(this.isPlaying != true){
			this.content = [];
		
	}else{
		console.log("Cant Clear Playlist during playback");
	}
	
};

Seamless.prototype.getPlaylist = function(){
	return this.content;
};


Seamless.prototype.removePlaylistElement = function(position){
	if(position != undefined){
		console.log("Removing Element" + this.content[position] );
		this.content.splice(position,1);
		return true;
	}else{
		if(this.content.length > 0){
			console.log("Removing Element" + this.content[this.content.length-1] );
			this.content.pop();		
			return true;			
		}else{
			console.log("Playlist is already empty");
			return false;
		}
	}
};

Seamless.prototype.addPlaylistElement = function(url,position){
	if(url != undefined){
		if(position != undefined){
			this.content.splice(position,1,url);
		}else{
			this.content.push(url);
		}	
		return true;
	}else{
		console.log("Failed to add Element. Provide the URL of the Element");
		return false;
	}
}

Seamless.prototype.setRectangleArea = function(x,y,w,h){
	
	if(x != undefined && y != undefined && w != undefined && h != undefined){
		
		this.rectangleCoordenateX = x;
		this.rectangleCoordenateY = y;
		this.rectangleWidth = w;
		this.rectangleHeight = h;
		return true;
	}else{
		console.log("Missing Arguments");
		return false;
	}
	
};



Seamless.prototype.play = function(index){
	if(index!=undefined && index < this.content.length && index >= 0){
		this.index=index;
	}else{
		console.log("PLaylist start parameter is out of bound. PLaylist will start from the begining");	
	}		
	
	if(this.content.length>0){
		//console.log('Reproducing: '+this.content[0]);
		console.log('open status: '+this.obj1.open(this.content[this.index]));
		this.index++;
		if(this.index > this.content.length-1){
			this.index=0;
		}
		console.log('setListener status: '+this.obj1.setListener(this.listener2));
		console.log('mixbuffer set: '+this.obj1.setStreamingProperty("USE_VIDEOMIXER"));
		console.log('prepare status: '+this.obj1.prepare());
		this.obj1.setDisplayRect(this.rectangleCoordenateX, this.rectangleCoordenateY, this.rectangleWidth, this.rectangleHeight);
		console.log('frame set: '+this.obj1.setStreamingProperty("SET_MIXEDFRAME"));
		console.log('play status: '+this.obj1.play());	
		this.isPlaying = true;
	}else{
		console.log("Content list is empty. Add content with obj.addPlaylistElement(url,position) ");
		
	}
};

Seamless.prototype.stop = function(){
	this.isPlaying = false;
	this.obj1.close();
	this.obj2.close();
};



Seamless.prototype.continuePlaying = function(){
    //Pausing the content
    console.log("Resuming Seamless..." + this.currentObj );
    if(this.currentObj == 1){
        this.obj1.play();
        console.log("First Resuming");
        console.log(this.obj1);
    }
    else if(this.currentObj == 2){
        this.obj2.play();
        console.log("Second Resuming");
        console.log(this.obj2);
    }
    console.log("...Done");
};

Seamless.prototype.pause = function(){
    //Pausing the content
    console.log("Pausing Seamless..." + this.currentObj );
    if(this.currentObj == 1){
        console.log("Fist");
        this.obj1.pause();
        console.log(this.obj1);
    }
    else if(this.currentObj == 2){
        this.obj2.pause();
        console.log("Second");
        console.log(this.obj2);
    }
    console.log("...Done");
};





Seamless.prototype.rotation = function(rotation){
    //Pausing the content
	
	console.log("...Done");
	switch(rotation){
	
	case 90:
		this.obj1.setDisplayRotation("PLAYER_DISPLAY_ROTATION_90");
		this.obj2.setDisplayRotation("PLAYER_DISPLAY_ROTATION_90");
		break;
	case 180:
		this.obj1.setDisplayRotation("PLAYER_DISPLAY_ROTATION_180");
		this.obj2.setDisplayRotation("PLAYER_DISPLAY_ROTATION_180");
		break;
	case 270:
		this.obj1.setDisplayRotation("PLAYER_DISPLAY_ROTATION_270");
		this.obj2.setDisplayRotation("PLAYER_DISPLAY_ROTATION_270");
		break;
	default:
		this.obj1.setDisplayRotation("PLAYER_DISPLAY_ROTATION_180");
		this.obj2.setDisplayRotation("PLAYER_DISPLAY_ROTATION_180");
		break;
	
	}
   
    
};



